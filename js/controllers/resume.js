RS.controller('Resume', ['$scope', '$http', function($scope, $http) {
    var R = this;

    var info = $http.get('content/userinfo.json').success(function(content) {
        R.name = content.name;
        R.tagline = content.tagline;
        R.profile = content.profile;
        R.skills = content.skills;
        R.applications = content.applications;
        R.education = content.education;
        R.experience = content.experience;
    });
}]);