var RS = angular.module('ResumeApp', ['ngRoute', 'ngResource'],
    function($routeProvider) {
        $routeProvider.when('/resume', {
            templateUrl: 'views/resume.html',
            controller: "Resume",
            controllerAs: 'resume'
        });
});

function MainCtrl ($location) {
    $location.path('resume')
}